#configuracion SW5

conf t
hostname S5
no ip domain-lookup

#puertos trunk
config ter
int f0/3
switchport mode trunk
switchport trunk allowed vlan all
exit
exit

#vtp
config ter
vtp domain g6
vtp password g6 
vtp version 2
vtp mode client
exit

#puertos modo acceso
config ter
int f0/1
switchport mode access
switchport access vlan 36
exit
int f0/2
switchport mode access
switchport access vlan 46
exit
exit

# SEGURIDAD
## puertos a blackhole
conf t
int range f0/4 - 24
switchport mode access
switchport access vlan 999
exit
exit

# vlan nativa
conf t
int f0/3
switchport trunk native vlan 99
exit
exit

copy running-config startup-config

#S5
conf t
int range f0/1
switchport port-security
switchport port-security maximum 1
switchport port-security violation shutdown
switchport port-security mac-address sticky