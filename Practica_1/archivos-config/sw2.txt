#configuracion SW2

conf t
hostname S2
no ip domain-lookup

#puertos trunk
config ter
int range f0/5 - 8
switchport mode trunk
switchport trunk allowed vlan all
exit
exit

#vtp
config ter
vtp domain g6
vtp password g6 
vtp version 2
vtp mode client
exit

#puertos modo acceso
config ter
int range f0/1 - 2
switchport mode access
switchport access vlan 16
exit
int range f0/3 - 4
switchport mode access
switchport access vlan 26
exit
exit

#po2
config ter
int range f0/5 - 6
channel-protocol lacp
channel-group 2 mode passive
exit
int range f0/7 - 8
channel-protocol lacp
channel-group 3 mode active
exit
exit

# SEGURIDAD
## puertos a blackhole
conf t
int range f0/9 - 24
switchport mode access
switchport access vlan 999
exit
exit

# vlan nativa
conf t
int port-channel2
switchport trunk native vlan 99
exit
int port-channel3
switchport trunk native vlan 99
exit
exit

#conf t
#int range f0/5 - 8
#switchport trunk native vlan 99
#exit
#exit

## port-security
conf t
int range f0/1-2
switchport port-security
switchport port-security maximun 5
exit
exit

copy running-config startup-config

#S2
conf t
int range f0/3 - 4
switchport port-security
switchport port-security maximum 1
switchport port-security violation shutdown
switchport port-security mac-address sticky