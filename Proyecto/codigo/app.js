const express = require('express');
const os = require('os');
const query = require('./db');
const app = express();
const port = 3000;

app.set('view engine', 'ejs');
app.use(express.static('public'));

app.get('/', async (req, res) => {
  res.send('Hello World!');
});

app.get('/index', async (req, res) => {

  const images = await query('select * from image');
  const resultImages = Object.values(JSON.parse(JSON.stringify(images)));

  let galery = [];
  let carrousel = [];

  for(let i = 0; i < resultImages.length; i++) {
    if(i < 10) {
      galery.push(resultImages[i]);
    } else {
      carrousel.push(resultImages[i]);
    }
  }

  let hostname = os.hostname();
  //console.log(resultImages);
  res.render('pages/index', {hostname: hostname, images: galery, home: carrousel});
});

app.get('/developers', async (req, res) => {
  
  const devs = await query('select * from developer');
  const admins = await query('select * from admin');
  const resultDevs = Object.values(JSON.parse(JSON.stringify(devs)));
  const resultAdmins = Object.values(JSON.parse(JSON.stringify(admins)));
  let hostname = os.hostname();

  res.render('pages/developers', {hostname: hostname, devs: resultDevs, admins: resultAdmins});
});

app.get('/administrators', async (req, res) => {
  
  const devs = await query('select * from developer');
  const admins = await query('select * from admin');
  const resultDevs = Object.values(JSON.parse(JSON.stringify(devs)));
  const resultAdmins = Object.values(JSON.parse(JSON.stringify(admins)));
  let hostname = os.hostname();

  res.render('pages/administrators', {hostname: hostname, devs: resultDevs, admins: resultAdmins});
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
});

/*

RowDataPacket {
    iddeveloper: 1,
    nombre: 'Julio Emiliano Cifuentes Zabala',
    puesto: 'Desarrollador',
    carnet: '201801677',
    curso: 'Redes de Computadoras 2'
  },

*/ 