
const express = require('express');
const os = require('os');
const query = require('./db');
const app = express();
const port = 3000;


app.get('/', async (req, res) => {
  res.send('Hello World!');
});

app.get('/imagenes', async (req, res) => {

  const images = await query('select * from image');
  const resultImages = Object.values(JSON.parse(JSON.stringify(images)));

  let galery = [];
  let carrousel = [];

  for(let i = 0; i < resultImages.length; i++) {
    if(i < 10) {
      galery.push(resultImages[i]);
    } else {
      carrousel.push(resultImages[i]);
    }
  }

  let hostname = os.hostname();
  let resultados = {data: images, hostname: hostname}
  //console.log(resultImages);
  res.json(resultados);
});

app.get('/developers', async (req, res) => {
  
  const devs = await query('select * from developer');
  const admins = await query('select * from admin');
  const resultDevs = Object.values(JSON.parse(JSON.stringify(devs)));
  const resultAdmins = Object.values(JSON.parse(JSON.stringify(admins)));
  let hostname = os.hostname();

  let resultados = {data: {devs: resultDevs, admins: resultAdmins}, hostname: hostname}
  res.json(resultados);
});

app.get('/administrators', async (req, res) => {
  
  const devs = await query('select * from developer');
  const admins = await query('select * from admin');
  const resultDevs = Object.values(JSON.parse(JSON.stringify(devs)));
  const resultAdmins = Object.values(JSON.parse(JSON.stringify(admins)));
  let hostname = os.hostname();

  res.render('pages/administrators', {hostname: hostname, devs: resultDevs, admins: resultAdmins});
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
});
