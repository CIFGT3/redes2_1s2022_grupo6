
const mysql = require('mysql');

const pool = mysql.createPool({
    host: '10.0.151.169',
    port: 3306,
    user: 'dev',
    password: '12345678',
    database: 'redes2'
});


let query = function( sql, values ) {
    // devolver una promesa
 return new Promise(( resolve, reject ) => {
   pool.getConnection(function(err, connection) {
     if (err) {
       reject( err )
     } else {
       connection.query(sql, values, ( err, rows) => {

         if ( err ) {
           reject( err )
         } else {
           resolve( rows )
         }
                    // finaliza la sesión
         connection.release()
       })
     }
   })
 })
}

module.exports = query;