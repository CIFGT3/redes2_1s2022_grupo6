# Manual Técnico

## Creación de vpc
### Configuración inicial

 - Para la creación de la VPC primero seleccionamos la opción para crear no solo la VPC sino tambien subredes, tablas de enrutamiento, NAT, etc.
 - Para la generación automática de etiquetas elegimos proyectoFase2
 - En el bloque de CIDR IPv4 seleccionamos 10.0.0.0/16 la cual nos brindara una gran cantidad de ip's.

![enter image description here](https://i.gyazo.com/3aaf5df641e75cf6ff7e30dab6d19162.png)

 - En zonas de disponibilidad solo necesitaremos una.
 - Seleccionamos una subred pública y dos privadas debido a la arquitectura solicitada.
 - Solicitamos un Gateway NAT para las puertas de enlace.
![enter image description here](https://i.gyazo.com/4c1c8fd9661f90889b68d4e17673dc19.png)
### Como resultado final de la configuración antes mencionada obtenemos:
 - una vpc con nombre proyectoFase2-vpc
 - tres subredes, una publica y dos privadas
 - tres tablas de enrutamiento
 - dos conexiones de red, una para el internet gateway y el otro para la NAT

![enter image description here](https://i.gyazo.com/1dbb0961a4f6cfac42e48de696a2de85.png)

## Creación de EC2
### Lanzamiento de instancias
La configuración base de todas las EC2 sera de una imágen ubuntu con la versión 20.04
![enter image description here](https://i.gyazo.com/54fa956615fdbf7507eafa2ebe92eb8c.png)

### EC2 para el cliente
 1. Para las primeras dos instancias que contienen la parte del cliente de la aplicación asignaremos la vpc creada anteriormente con nombre  proyectoFase2-vpc.
 2. Las asignamos a la primera subred privada 
![enter image description here](https://i.gyazo.com/1c4f58c0aeca4efa7246aa908920fc16.png)

### EC2 para el servicios de servidor y base de datos

 1. Las siguientes tres instancias contienen la aplicación que contiene los servicios y que conectan tambien con la base de datos.
 2. Estas tres instancias seran asignadas a una segunda red privada.
![enter image description here](https://i.gyazo.com/520ed731f20cf0fc99352c2e283c1103.png)

## Configuración de Application Load Balancer

 El load balancer se le asigna un nombre y le indicamos que tendrá acceso a internet y será por medio de ipv4.
![enter image description here](https://i.gyazo.com/e990f0250e59b7254a301d5d1e2922c6.png)
Ahora le indicamos que el load balancer estara conectado a la vpc que creamos anteriormente proyectoFase2-vpc y que estara en la misma zona de disponibilidad, tambien esta conectada a la subred pero esta es de tipo publica.
![enter image description here](https://i.gyazo.com/e0132d9b954bf0234e1fdaa8306214f6.png)
Indicamos tambien que estará conectada a las instancias EC2 que creamos anteriormente.
![enter image description here](https://i.gyazo.com/8a5cd491b06dbc4f77b8b57a0059c8f9.png)
Asignamos un nombre a donde estara apuntando nuestro load balancer y como este tendra un certificado de seguridad, entonces el protocolo sera https por el puerto 443
![enter image description here](https://i.gyazo.com/1d618f13a58207d36b75438a9e93592f.png)
como anteriormente se menciono, este load balancer estara conectado al las dos EC2 que contienen la aplicación del cliente 
![enter image description here](https://i.gyazo.com/2167955e1258f7bdfcc6a2a51701e997.png)
Por ultimo agregamos el certificado SSL a nuestro application load balancer
![](https://i.gyazo.com/2e0da524a4b7ddfef28fd43b0f6e722f.png)

## Certificado SSL
Para la solicitud de certificado SSL se utilizaron los dominios de:

 1. ucron-r2-g6.tk
 2. ucron-r2-g6.ml
![enter image description here](https://i.gyazo.com/71fc46f5f7dcd6b44a3f3ecec4957787.png)
Estos se agregaron a la configuración CNAME en la configuración de DNS desde FREENOM donde se obtuvieron los dominios
![enter image description here](https://i.gyazo.com/f97025785664e6731111b660bdef2dca.png)

Ambos dominios obtienen el certificado
![enter image description here](https://i.gyazo.com/188b1155e8e69b1e27334a99d8e26a72.png)

## Grupos de Seguridad
para el grupo de seguridad del load balancer permitimos unicamente conexiones mediante https por el puerto 443 donde cualquier ipv4 podra acceder a esta y ver el sitio web.
![enter image description here](https://i.gyazo.com/27c807647f92cefccb0d168e9d35b397.png)

## Instalación de MySql
[Instalación de mysql en ubuntu 20.04](https://www.digitalocean.com/community/tutorials/how-to-install-mysql-on-ubuntu-20-04-es)
![enter image description here](https://i.gyazo.com/ea1c132aa725a6252c3b17b8815c5bb3.png)

## Instalación de node
[Instalación de nodejs](https://www.digitalocean.com/community/tutorials/how-to-install-node-js-on-ubuntu-20-04-es)
![enter image description here](https://i.gyazo.com/da6e1707b23b06a5d0ef738d1bf7d6c2.png)

## Instalación de apache
[Instalación de apache](https://www.digitalocean.com/community/tutorials/how-to-install-the-apache-web-server-on-ubuntu-20-04-es)
![enter image description here](https://i.gyazo.com/2a5e015a9f3ac369f67d08c40a1a371e.png)
